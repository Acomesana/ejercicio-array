'use strict'

// puntuaciones
const puntuaciones = [
  {
    equipoA: "Toros Negros",
    puntos: [1, 3, 4, 2, 10, 8],
  },
  {
    equipoB: "Amanecer Dorado",
    puntos: [8, 5, 2, 4, 7, 5, 3],
  },
  {
    equipoC: "Águilas Plateadas",
    puntos: [5, 8, 3, 2, 5, 3],
  },
  {
    equipoD: "Leones Carmesí",
    puntos: [5, 4, 3, 1, 2, 3, 4],
  },
  {
    equipoE: "Rosas Azules",
    puntos: [2, 1, 3, 1, 4, 3, 4],
  },
  {
    equipoF: "Mantis Verdes",
    puntos: [1, 4, 5, 1, 3],
  },
  {
    equipoG: "Ciervos Celestes",
    puntos: [3, 5, 1, 1],
  },
  {
    equipoH: "Pavos Reales Coral",
    puntos: [2, 3, 2, 1, 4, 3],
  },
  {
    equipoI: "Orcas Moradas",
    puntos: [2, 3, 3, 4],
  },
];

puntuaciones.forEach((equipos) => {
  let marcador = 0;
  equipos.puntos.map(punto => {
    marcador = marcador + punto;
  })
  equipos.puntos = marcador;

})

function comparar(a, b) { return b.puntos - a.puntos; }

puntuaciones.sort(comparar);

console.log(puntuaciones[0]);
console.log(puntuaciones[puntuaciones.length - 1]);